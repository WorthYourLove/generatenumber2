package com.example.homework222

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()

    }


    @SuppressLint("SetTextI18n")
    private fun init() {
        val generateRandomNumberButton = findViewById<Button>(R.id.generateRandomNumber2)
        val randomNumberTextView = findViewById<TextView>(R.id.randomNumberTextView)

        generateRandomNumberButton.setOnClickListener {
            val number: Int = randomNumber()
            d("randomNumber", "This is random Number")
            if ((number % 5 == 0) and (number > 0)) {
                randomNumberTextView.text = "Yes"
            } else randomNumberTextView.text = "No"
        }

    }

    private fun randomNumber() = (-100..100).random()
}